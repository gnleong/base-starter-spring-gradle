FROM gradle:alpine AS BUILD
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon -Pvaadin.productionMode

FROM eclipse-temurin:17

EXPOSE 8080

RUN mkdir /app

COPY --from=BUILD /home/gradle/src/build/libs/*.jar /app/
COPY --from=BUILD /home/gradle/src/build.gradle /app/build.gradle
COPY --from=BUILD /home/gradle/src/package.json /app/package.json

CMD ["/bin/bash", "-c", "find -type f -name '*.jar' | xargs java -jar"]
